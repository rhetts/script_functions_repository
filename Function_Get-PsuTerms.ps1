# Written for me by Sheree Grier

Function Get-PsuTerms {
    
    Param([string]$Filter)
    
    $Now      = (Get-Date)
    $Terms    = @()
    $TermDefs = @()
    
    # Define each term's name, sequence number, and beginning/ending months.
    $TermDefs += New-Object PSObject -Property @{Number=1;MonthsBegin='11|12|01';MonthsEnd='01|02|03';Name='Winter';}
    $TermDefs += New-Object PSObject -Property @{Number=2;MonthsBegin='02|03|04';MonthsEnd='04|05|06';Name='Spring'}
    $TermDefs += New-Object PSObject -Property @{Number=3;MonthsBegin='05|06|07';MonthsEnd='07|08|09';Name='Summer'}
    $TermDefs += New-Object PSObject -Property @{Number=4;MonthsBegin='08|09|10';MonthsEnd='10|11|12';Name='Fall'}

    # Look for lines in PSU's academic calendar that contain 'classes begin/end ... MM/dd/YYYY'
    $page = Invoke-WebRequest 'https://www.pdx.edu/registration/calendar#/'
    [regex]::matches($page.Content, '(?i)Classes (begin|end)(\s\(.+\))*.+\d\d/\d\d/\d{4}') | %{
   
        # Convert the academic calendar into a PowerShell object.
        $x        = $_.Value.ToLower() -Split '\<.+\>'
        $date     = Get-Date $x[1] #-f 'yyyy-MM-dd'
        $month    = Get-Date $x[1] -f 'MM'
        $year     = [int](Get-Date $x[1] -f 'yyyy')
        $boundary = ($x[0] -Split ' ')[1]
        $prevTerm = $term
        $term     = $TermDefs | Where { $_."Months$boundary" -match $month }

        Switch ($term -ne $prevTerm) {
            $True { $Terms += New-Object PSObject -Property @{Name=$term.Name;Year=$year;Number=$term.Number;Begin=$date;End=''}}
           $False { $Terms[-1].End    = $date.AddSeconds(86399) }
        }
    }

    # Filter the academic terms depending on whether a switch was used.
    Switch ($Filter) {
        ThisTerm { $Terms = $Terms | Where {$_.Begin -le $Now -And $_.End -ge $Now} }
        LastTerm { $Terms = $Terms | Where {$_.End -lt $Now} | Sort End -Desc | Select -First 1 }
        NextTerm { $Terms = $Terms | Where {$_.Begin -gt $Now} | Sort Begin | Select -First 1 }
        
        # PaperCut billing cycles end one second before the next
        # term starts--adjust start/end times before filtering.
        PaperCut { For ($j=0;$j -le $Terms.Count-2;$j++) {
                        $Terms[$j].Begin = $Terms[$j].Begin.AddHours(8)
                        $Terms[$j].End = $Terms[$j+1].Begin.AddSeconds(28799)
                   }
                   $Terms = $Terms | Where {$_.End -lt $Now} | Sort End -Desc | Select -First 1
                 }
    }
 
    $AcademicTermCode = "{0:d4}{1:d2}" -f $year,$terms.number
    Return $AcademicTermCode
    #Return $Terms

}