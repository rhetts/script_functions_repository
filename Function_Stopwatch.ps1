Write-Debug -Message "Starting Timer/stopwatch feature of powershell"
$stopwatch = [System.Diagnostics.Stopwatch]::StartNew()
# Insert Code to run that needs to be timed
Write-Information -Message "Importing done in $($sw.Elapsed.TotalSeconds.ToString('n2')) seconds."
Write-Debug -Message "Stopping Stopwatch"
$Stopwatch.stop()
