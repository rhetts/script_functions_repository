<#
.Synopsis
{Name or Brief overview}
.DESCRIPTION
{Detailed description of the script}
.EXAMPLE
{How to run script}
.INPUTS
{Any inputs the script is expecting}
.OUTPUTS
{What and where the output would be}
.FUNCTIONALITY
{What does it do?}
#>
####################### Variables for script #######################

$LogPath = "    {Log's Path}    "
$LogName = "$(Get-Date -f yyyyMMdd)_    {Name of Log}    .log"
$LogFile = $LogPath + $LogName

$BlankLine = ' '
$TodaysDate = (Get-Date).ToLongDateString()

$EmailFrom = "no-reply@pdx.edu"
$EmailTo = "alert-bas@pdx.edu"
$PSEmailServer = "mailhost.pdx.edu"

####################### Functions #######################

function Write-Log {
    Param(
        $Message
    )
    function TS {Get-Date -Format 'HH:mm:ss'}
    "[$(TS)]     $Message" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
}


Function Get-LogStatus {
    # Checks to see if a log file exists and if not creates one
	# This function requires the Write-Log Function to be run first.
    If (Test-Path $LogFile) {
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        "               $TodaysDate" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        Write-Log "     {Initial Log Entry}     "
    }
    Else {
        New-Item $LogFile -Force -ItemType File
        "               $TodaysDate" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        Write-Log Write-Log "     {Initial Log Entry}     "
    }
}


Function Send-EmailSuccess {
    # Variables needed for this function
    $EmailSubject = ""
    $EmailBody = ""
    ####################################
    Write-Host "";
    Write-Log "";
    Send-MailMessage -From $EmailFrom -To $EmailTo -Subject $EmailSubject -Body $EmailBody -SmtpServer $PSEmailServer
    #
    ## Helpful Tip ##
    # The `n codes a new line.
    # A commna at the end of a line lets you continue on the new line.
    # I.e. "This text has a newline`n",
    # I.e. "in it."
    ################
}


Function Send-EmailError {
    # Variables needed for this function
    $EmailSubject = ""
    $EmailBody = ""
    ####################################
    Write-Host "";
    Write-Log "";
    Send-MailMessage -From $EmailFrom -To $EmailTo -Subject $EmailSubject -Body $EmailBody -SmtpServer $PSEmailServer
    #
    ## Helpful Tip ##
    # The `n codes a new line.
    # A commna at the end of a line lets you continue on the new line.
    # I.e. "This text has a newline`n",
    # I.e. "in it."
    ################
}



####################### Commands #######################
$Error.Clear()
Get-LogStatus
Send-EmailSuccess
Send-EmailError
